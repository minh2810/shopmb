@extends('admin.layout')
@section('header')
    <li class="hidden-xs"><a href="/admin/coupon_all">Mã giảm giá</a></li>
    <li class="title-ellipsis"><a>Danh sách</a></li>
@endsection
@section('content')
    <section class="clearfix">
        <div class="box box-table">
            <div class="row box-header title-tab"><h3 class="box-title">DANH SÁCH</h3></div>
            <div class="box-body">
                <div class="action-box hidden">
                    <div class="dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><i
                                    class="fa fa-share"></i> Chọn thao tác (đang chọn<span class="num-select"></span>
                            liên hệ)  <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a data-value="read" class="contact-status">Đã đọc</a></li>
                            <li><a data-value="unreply" class="contact-status">Chưa phản hồi</a></li>
                            <li><a data-value="reply" class="contact-status">Đã phản hồi</a></li>
                            <li><a data-value="delete" class="contact-status">Xóa</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="dataTables_length" id="DataTables_Table_0_length"><label>Hiển
                            thị
                            <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                                    class="form-control input-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> dữ liệu</label></div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Tìm
                            kiếm:
                            <input name="search" class="form-control input-sm" placeholder="Nhập từ khóa cần tìm..."
                                   aria-controls="DataTables_Table_0"></label></div>
                </div>
                <div class="box-body">
                    <div class="tab-content">
                        <div class="form-group">
                            <div class="col-lg-2 col-md-6">
                                <strong>Chỉ hiện</strong>
                            </div>
                            <div class="col-lg-10 col-md-6">
                                <a href="/admin/coupon_all?sort=only-deleted"><span
                                            class="label label-warning">Đang hiện</span> </a>|
                                <a href="/admin/coupon_all?sort=only-deleted"><span
                                            class="label label-success">Đã hết hạn</span> </a>|
                                <a href="/admin/contact_all?sort=only-hiden"><span
                                            class="label label-danger">Đã ẩn</span> </a>|
                                <a href="/admin/coupon_all"><span class="label label-info">Tất cả</span>
                                </a>
                            </div>
                        </div>
                        <div class="form-group" id="actions" style="display: none">
                            <div class="col-lg-2 col-md-6">
                                <strong>Chuyển tất cả thành </strong>
                            </div>
                            <div class="col-lg-10 col-md-6">
                                <a href="/admin/coupon_all?sort=only-deleted"><span
                                            class="label label-warning">Đang hiện</span> </a>|
                                <a href="/admin/coupon_all?sort=only-deleted"><span
                                            class="label label-success">Đã hết hạn</span> </a>|
                                <a href="/admin/contact_all?sort=only-hiden"><span
                                            class="label label-danger">Đã ẩn</span> </a>|
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                            <th><input type="checkbox" id="checkall"/></th>
                            <th>#id</th>
                            <th>Tên</th>
                            <th>Mã code</th>
                            <th>Giá trị</th>
                            <th>Ngày bắt đầu</th>
                            <th>Ngày kết thúc</th>
                            <th>Trạng thái</th>
                            </thead>
                            <tbody>
                            @foreach ($coupons as $coupon)
                                <tr role="row">
                                    <td><input type="checkbox" value="{{ $coupon->id }}"
                                               class="checkthis action"/></td>
                                    <td><a href="/admin/coupon?id={{ $coupon->id }}">#{{ $coupon->id }}</a></td>
                                    <td class="all"><a
                                                href="/admin/coupon?id={{ $coupon->id }}">{{ $coupon->title }}</a></td>
                                    <td>{{ $coupon->code }}</td>
                                    <td>{{ Helper::formatMoney($coupon->discount)}} VNĐ </td>
                                    <td>{{ $coupon->start_date }}</td>
                                    <td>{{ $coupon->end_date }}</td>
                                    <td>                     @if ($coupon->status == 'inactive')
                                            <label class="label label-warning">{{ $coupon->status }}</label>
                                        @elseif ($coupon->status == 'active')
                                            <label class="label label-success">{{ $coupon->status }}</label>
                                        @elseif ($coupon->status == 'outdate')
                                            <label class="label label-danger">{{ $coupon->status }}</label>
                                        @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="col-lg-4 col-md-4 pull-left">
                            <button type="button" class="btn btn-success" name="button"
                                    onclick="window.location.href='/admin/createCoupon'">
                                Thêm mã giảm giá
                            </button>
                        </div>
                        <div class="col-lg-8 col-md-8 pull-right">
                            <ul class="pagination pull-right">
                                <li class="disabled"><a href="#"><span
                                                class="glyphicon glyphicon-chevron-left"></span></a></li>
                                @for ($i=1; $i <= $total_page; $i++)
                                    <li class="
                  @if ($page == $i)
                                            active
@endif
                                            "><a
                                                href="/admin/order_all?page={{$i}}&perpage={{$perpage}}">{{$i}}</a>
                                    </li>
                                @endfor
                                <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $("#mytable #checkall").click(function () {
            if ($("#mytable #checkall").is(':checked')) {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", true);
                });
            } else {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
        $("[data-toggle=tooltip]").tooltip();
        $('input.action, #checkall').on('click', function (e) {
            var action = false;
            $.each($('input.action'), function (i, e) {
                if ($(e).prop('checked')) {
                    action = true;
                    return;
                }
            })
            if (action) {
                $('#actions').css('display', 'block');
            } else {
                $('#actions').css('display', 'none');
            }
        })
    </script>
@endsection