<html>
<head><title>Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/resources/views/admin/libs/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/views/admin/libs/AdminLTE.min.css">
    <link rel="stylesheet" href="/resources/views/admin/libs/toastr.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="/resources/views/admin/libs/jquery.min.js"></script>
    <script src="/resources/views/admin/libs/toastr.min.js"></script>
    <style>.login-box {
            width: 300px;
            margin: 100px auto 0;
            text-align: center;
        }

        .btn-login {
            background-color: $ main_color !important;
            box-shadow: none !important;
            border: none !important;
            outline: 0 !important;
        }</style>
</head>
<body>

<div class="login-box"><h3>ĐĂNG NHẬP</h3>
    <div class="login-box-body">
        <form >
            <div class="form-group has-feedback"><input type="email" placeholder="Email" name="email" required=""
                                                        class="form-control"><span
                        class="glyphicon glyphicon-envelope form-control-feedback"></span></div>
            <div class="form-group has-feedback"><input type="password" placeholder="Password" name="password"
                                                        required="" class="form-control"><span
                        class="glyphicon glyphicon-lock form-control-feedback"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat btn-login">Đăng Nhập</button>
                    <a href="/user/forgotpassword" style="display: block; margin-top: 5px;text-align:right">Quên mật
                        khẩu?</a></div>
            </div>
        </form>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
    $('.btn-login').on('click', function (e) {
        e.preventDefault();
        var email = $('input[name=email]').val();
        var password = $('input[name=password]').val();
        var params = {
            type: 'POST',
            url: '/admin/login',
            data: {
                email: email,
                password: password
            },
            success: function (result) {
                result = $.parseJSON(result);
                console.log(result);
                if (result.code == 0) {
                    window.location.href = '/admin';
                } else {
                    $('.login-result').text("Email hoặc mật khẩu không khớp!");
                }
            }
        }
        $.ajax(params);
    })
</script>

</body>

</html>
