@extends('admin.layout')
@section('header')
    <li class="hidden-xs"><a href="/admin/coupon_all">Mã giảm giá</a></li>
    <li class="title-ellipsis"><a>Thêm mới</a></li>
@endsection
@section('button_save')
    <ol class="button-right col-md-4 col-sm-5 col-xs-12">
        <li><a class="btn btn-admin btn-create btn_create_coupon"><i class="fa fa-floppy-o"></i><i
                        class="fa fa-circle-o-notch fa-spin"></i>Lưu lại</a></li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="box ">
                <div class="form-horizontal form-product">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Tạo mã giảm giá
                            <div class="action_result">
                                <span style="color: red;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-2 col-md-12 control-label">Tiêu đề<strong
                                            class="required">*</strong></div>
                                <div class="col-lg-10 col-md-12">
                                    <input name="title" placeholder="Tên mã giảm"
                                           class="form-control title text-overflow-title">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-2 col-md-12 control-label">Mô tả<strong
                                            class="required">*</strong></div>
                                <div class="col-lg-10 col-md-12">
                                        <textarea id="description" name="description" rows="8" cols="80"
                                                  placeholder="Mô tả"
                                                  class="tinymce form-control title text-overflow-title"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">ĐIỀU KIỆN SỬ DỤNG</div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-sm-2 control-label">Đơn hàng tối thiểu<strong
                                            class="required">*</strong></div>
                                <div class="col-sm-10"><input name="min_value_order"
                                                              placeholder="Giá trị đơn hàng tối thiểu" min="0" value="0"
                                                              onkeypress="inputPositiveNumbers( event )"
                                                              class="form-control formatMoney"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2 control-label">Từ ngày<strong class="required">*</strong></div>
                                <div class="col-sm-10">
                                    <div class="input-group"><input name="start_date" placeholder="dd-mm-yyyy"
                                                                    class="form-control datepicker inputmark-date"><span
                                                class="input-group-addon between-datetime">đến ngày<strong
                                                    class="required">*</strong></span><input name="end_date"
                                                                                             placeholder="dd-mm-yyyy"
                                                                                             class="form-control datepicker inputmark-date">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">MÃ GIẢM GIÁ</div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="text-title">Mã giảm giá<strong class="required">*</strong></div>
                                <input name="code" placeholder="Mã" class="form-control"></div>
                            <div class="form-group">
                                <div class="text-title">Số lượng<strong class="required">*</strong></div>
                                <input name="usage_left" placeholder="Số lượng" type="number" min="0" value="0"
                                       onkeypress="inputPositiveNumbers(event)" class="form-control"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">GIÁ TRỊ</div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12">
                            {{--<div class="form-group">--}}
                            {{--<div class="text-title">Thể loại<strong class="required">*</strong></div>--}}
                            {{--<select name="type" class="form-control">--}}
                            {{--<option value="value">Giảm theo đơn giá</option>--}}
                            {{--<option value="percent">Giảm theo phần trăm</option>--}}
                            {{--</select></div>--}}
                            <div class="form-group">
                                <div class="text-title">Giá trị<strong class="required">*</strong></div>
                                <input type="text" name="value" placeholder="Giá trị" min="0" value="0"
                                       onkeypress="inputPositiveNumbers(event)" class="form-control formatMoney"></div>
                            {{--<div class="form-group max-value-percent hidden">--}}
                            {{--<div class="text-title">Giá trị tối đa<strong class="required">*</strong></div>--}}
                            {{--<input name="max_value_percent" placeholder="Giá trị tối đa"--}}
                            {{--onkeypress="inputPositiveNumbers(event)" value="0"--}}
                            {{--class="form-control formatMoney"></div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">TRẠNG THÁI</div>
                    </div>
                    <div class="box-body"><select name="status" class="form-control">
                            <option value="active">Hiển thị</option>
                            <option value="inactive">Ẩn</option>
                        </select></div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            tinymce.init({
                selector: 'textarea.tinymce',
                plugins: 'codesample'
            });
            $(".datepicker").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                defaultDate: "+1w",
                changeMonth: true
            });

            $(".start-date").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                defaultDate: "+1w",
                changeMonth: true,
                onSelect: function (selected) {
                    var start_date = $(".start-date").datepicker("getDate");
                    $(".end-date").datepicker("option", "minDate", start_date);
                }
            }).on('clearDate', function (selected) {
                $('.end-date').datepicker("option", "minDate", 0);
            });

            $(".end-date").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                defaultDate: "+1w",
                changeMonth: true,
                onSelect: function (selected) {
                    var end_date = $(".end-date").datepicker("getDate")
                    $(".start-date").datepicker("option", "maxDate", end_date);
                }
            }).on('clearDate', function (selected) {
                $('.start-date').datepicker("option", "maxDate", null);
            });
        });
        $('.btn_create_coupon').on('click', function () {
            var data = {};
            data.title = $('input[name="title"]').val();
            data.description = tinymce.get('description').getContent();
            data.code = $('input[name="code"]').val();
            data.usage_left = $('input[name="usage_left"]').val();
            data.value = $('input[name="value"]').val();
            data.start_date = $('input[name="start_date"]').val();
            data.end_date = $('input[name="end_date"]').val();
            data.status = $('select[name="status"]').val();
            data.min_value_order = $('input[name="min_value_order"]').val();
            if (!data.title) {
                $('input[name="title"]').addClass('error');
                return toastr.error('Chưa nhập tiêu đề');
            }
            if (!data.description) {
                $('input[name="description"]').addClass('error');
                return toastr.error('Chưa nhập mô tã');
            }
            if (!data.code) {
                $('input[name="code"]').addClass('error');
                return toastr.error('Chưa nhập mã giảm giá');
            }
            if (!data.usage_left) {
                $('input[name="usage_left"]').addClass('error');
                return toastr.error('Chưa nhập số lượng mã giảm giá');
            }
            if (data.min_value_order <= 0) {
                $('input[name="min_value_order"]').addClass('error');
                return toastr.error('Giá trị đơn hàng tối thiểu không được nhỏ hơn 0');
            }
            if (!data.start_date) {
                $('input[name="start_date"]').addClass('error');
                return toastr.error('Chưa nhập ngày bắt đầu');
            }
            if (!data.end_date) {
                $('input[name="end_date"]').addClass('error');
                return toastr.error('Chưa nhập ngày kết thúc');
            }

            function dmy2ymd(date) {
                if (date.indexOf('-') > -1) {
                    var arr = date.split('-');
                    return arr[2] + '-' + arr[1] + '-' + arr[0];
                } else if (date.indexOf('/') > -1) {
                    var arr = date.split('/');
                    return arr[2] + '/' + arr[1] + '/' + arr[0];
                }
                return date;
            }

            data.start_date = dmy2ymd(data.start_date);
            data.end_date = dmy2ymd(data.end_date);
            if (new Date(data.end_date) < new Date(data.start_date)) {
                $('input[name="end_date"]').addClass('error');
                return toastr.error('Ngày kết thúc không được bé hơn ngày bắt đầu');
            }
            createCoupon(data);
        })

        function createCoupon(data) {
            $.ajax({
                type: 'POST',
                url: '/admin/createCoupon',
                data: data,
                success: function (json) {
                    console.dir(json);
                    if (!json.code) {
                        alert('Tạo mã giảm giá thành công');
                        window.location.href = '/admin/coupon_all';
                    } else if (json.code = -1) {
                        alert('Mã giảm đã tồn tại');
                    } else {
                        alert('Đã có lỗi xảy ra! Vui lòng thử lại sau!')
                    }
                }
            });
        }
    </script>
@endsection