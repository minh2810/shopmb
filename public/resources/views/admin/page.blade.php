@extends('admin.layout')
@section('header') <li class="hidden-xs"><a href="/admin/page_all">Trang nội dung</a></li>
<li class="title-ellipsis"><a>Danh sách</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="box box-info">
                <div class="form-horizontal form-product">
                    <div class="box-header">
                        <div class="box-title clearfix">
                            <div class="pull-left title-tab">Danh sách trang nội dung @if ($page_name == 'active')
                                    <span class="label label-success">Sẵn sàng</span>
                                @elseif ($page_name == 'deleted')
                                    <span class="label label-danger">Bị xóa</span>
                                @elseif ($page_name == 'hiden')
                                    <span class="label label-warning">Bị ẩn</span>
                                @else
                                    <span class="label label-info">Tất cả</span>
                                @endif
                                ({{$current_items}} trong tổng số {{$total_item}} nhóm)
                            </div>
                        </div>
                    </div>
                    <div class="box box-table">
                        <div class="box-body">
                            <div class="box-body">
                                <div class="tab-content">
                                    <div class="form-group">
                                        <div class="col-lg-2 col-md-6">
                                            <strong>Chỉ hiện</strong>
                                        </div>
                                        <div class="col-lg-10 col-md-6">
                                            <a href="/admin/page_all?sort=only-active"><span
                                                        class="label label-success">Sẵn
                                                    sàng</span> </a>|
                                            <a href="/admin/page_all?sort=only-deleted"><span
                                                        class="label label-danger">Đã
                                                    xóa</span> </a>|
                                            <a href="/admin/page_all?sort=only-hiden"><span class="label label-warning">Đã
                                                    ẩn</span> </a>|
                                            <a href="/admin/page_all"><span class="label label-info">Tất cả</span> </a>|
                                        </div>
                                    </div>
                                    <div class="form-group" id="actions" style="display: none">
                                        <div class="col-lg-2 col-md-6">
                                            <strong>Chuyển tất cả thành </strong>
                                        </div>
                                        <div class="col-lg-10 col-md-6">
                                            <a href="#" class="action_name" data-value="active"><span
                                                        class="label label-success">Sẵn sàng</span> </a>|
                                            <a href="#" class="action_name" data-value="deleted"><span
                                                        class="label label-danger">Đã xóa</span> </a>|
                                            <a href="#" class="action_name" data-value="hiden"><span
                                                        class="label label-warning">Đã ẩn</span> </a>|
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">


                                    <table id="mytable" class="table table-bordred table-striped">

                                        <thead>

                                        <th><input type="checkbox" id="checkall"/></th>
                                        <th>#id</th>
                                        <th>Tên trang</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Sửa</th>
                                        <th>Xóa</th>
                                        </thead>
                                        <tbody>
                                        @foreach ($pages as $page_current)
                                            <tr class="item" id="{{$page_current->id}}">
                                                <td><input type="checkbox" value="{{$page_current->id}}"
                                                           class="checkthis action"/>
                                                </td>
                                                <td>{{$page_current->id}}</td>
                                                <td>
                                                    <a href="/admin/page?id={{$page_current->id}}">{{$page_current->title}}</a>
                                                </td>
                                                <td>{{$page_current->created_at}}</td>
                                                <td class="status">
                                                    @if ($page_current->status == 'active')
                                                        <span class="label label-success">Sẵn sàng</span>
                                                    @elseif ($page_current->status == 'deleted')
                                                        <span class="label label-danger">Bị xóa</span>
                                                    @else
                                                        <span class="label label-warning">Bị ẩn</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                                        <button class="btn btn-show-edit btn-primary btn-xs"
                                                                data-title="Edit" data-id="{{$page_current->id}}"><span
                                                                    class="glyphicon glyphicon-pencil"></span></button>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p data-placement="top" data-toggle="tooltip" title="Delete">
                                                        <button class="btn btn-danger btn-xs btn-delete"
                                                                data-title="Delete"
                                                                data-id="{{$page_current->id}}" data-toggle="modal"
                                                                data-target="#delete"><span
                                                                    class="glyphicon glyphicon-trash"></span></button>
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                    <div class="col-lg-4 col-md-4 pull-left">
                                        <button type="button" class="btn btn-success" name="button"
                                                onclick="window.location.href='/admin/createPage'">
                                            Thêm trang nội dung
                                        </button>
                                    </div>
                                    <div class="col-lg-8 col-md-8 pull-right">
                                        <ul class="pagination pull-right">
                                            <li class="disabled"><a href="#"><span
                                                            class="glyphicon glyphicon-chevron-left"></span></a></li>
                                            @for ($i=1; $i <= $total_page; $i++)
                                                <li class="
                                @if ($page == $i)
                                                        active
@endif
                                                        ">
                                                    <a href="/admin/page_all?page={{$i}}&perpage={{$perpage}}">{{$i}}</a>
                                                </li>
                                            @endfor
                                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                            <h4 class="modal-title custom_align" id="Heading">Sửa sơ bộ</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="col-lg-4 col-md-12 control-label">Tựa trang<strong
                                                            class="required">*</strong></div>
                                                <div class="col-lg-8 col-md-12"><input name="title"
                                                                                       placeholder="Tên trang nội dung"
                                                                                       class="settings form-control title text-overflow-title">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-4 col-md-12 control-label">Trạng thái<strong
                                                            class="required">*</strong></div>
                                                <div class="col-lg-8 col-md-12">
                                                    <select class="form-control" name="status">
                                                        <option value="active">Sẵn sàng</option>
                                                        <option value="deleted">Bị xóa</option>
                                                        <option value="hiden">Bị ẩn</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer ">
                                            <button type="button" class="btn btn-primary btn-lg btn-quick-edit"
                                                    style="width: 100%;"><span
                                                        class="glyphicon glyphicon-ok-sign"></span> Update
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            $("#mytable #checkall").click(function () {
                if ($("#mytable #checkall").is(':checked')) {
                    $("#mytable input[type=checkbox]").each(function () {
                        $(this).prop("checked", true);
                    });
                } else {
                    $("#mytable input[type=checkbox]").each(function () {
                        $(this).prop("checked", false);
                    });
                }
            });
            $("[data-toggle=tooltip]").tooltip();
            $('.btn-delete').on('click', function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                var params = {
                    url: '/admin/deletePage',
                    type: 'POST',
                    data: {
                        id: id
                    },
                    success: function (result) {
                        if ($.parseJSON(result).code == 0) {
                            $('tr#' + id + '').find('td.status').html('<span class="label label-danger">Bị xóa</span>');
                            console.dir($('tr#' + id + '').find('td.status'));
                            alert("Xóa thành công trang nội dung");
                        }
                    }
                }
                $.ajax(params);
            })
            $('.btn-show-edit').on('click', function (e) {
                var id = $(this).data('id');
                $.ajax({
                    type: 'post', url: '/admin/getPageById', data: {id: id}, success: function (result) {
                        result = $.parseJSON(result);
                        $('input[name=title]').val(result.data.title);
                        $('select option[value=' + result.data.status + ']').attr('selected', 'selected');
                        $('#edit').attr('data-id', id);
                        $('#edit').modal('show');
                    }
                })
            })
            $('.btn-quick-edit').on('click', function (e) {
                e.preventDefault();
                var id = $('#edit').data('id');
                var title = $('input[name=title]').val();
                var status = $('select[name=status]').val();

                $.ajax({
                    url: '/admin/quickUpdatePage', type: 'POST', data: {
                        title: title,
                        status: status,
                        id: id
                    }, success: function (result) {
                        if ($.parseJSON(result).code == 0) {
                            window.location.reload(true);
                        }
                    }
                })
            })
            $('input.action, #checkall').on('click', function (e) {
                var action = false;
                $.each($('input.action'), function (i, e) {
                    if ($(e).prop('checked')) {
                        action = true;
                        return;
                    }
                })
                if (action) {
                    $('#actions').css('display', 'block');
                } else {
                    $('#actions').css('display', 'none');
                }
            })
            $('.action_name').on('click', function (e) {
                e.preventDefault();
                var status = $(this).data('value');
                var ids = $("input.action:checkbox:checked").map(function () {
                    return $(this).val();
                }).get();
                $.ajax({
                    url: '/admin/changeStatusPage',
                    type: "POST",
                    data: {ids: ids, status: status},
                    success: function (result) {
                        if ($.parseJSON(result).code == 0) {
                            console.dir($.parseJSON(result));
                            window.location.reload(true);
                        }
                    }
                })
            })
        });
    </script>
@endsection
