@extends('admin.layout')
@section('header') Khách hàng
@endsection
@section('content')
    <style media="screen">
        .block {
            margin-bottom: 20px;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 block">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">Thông tin khách hàng</div>
                    </div>
                    <div class="box-body">
                        <section class="">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#tab-info">THÔNG TIN</a></li>
                                <li class=""><a data-toggle="pill" href="#tab-order">DANH SÁCH ĐƠN HÀNG</a></li>
                            </ul>
                            <div class="tab-content">
                                @php
                                    $counter = 0;
                                @endphp
                                @foreach ($orders as $order)
                                    @php
                                        $counter++;
                                    @endphp
                                    @break($counter > 1)
                                    <div id="tab-info" class="tab-pane fade active in">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="box">
                                                <div class="form-horizontal">
                                                    <div style="padding-top: 30px;" class="box-body">
                                                        <div class="form-group">
                                                            <div class="col-sm-2 control-label">Họ tên<strong
                                                                        class="required">*</strong></div>
                                                            <div class="col-sm-10"><input name="name"
                                                                                          placeholder="Họ tên"
                                                                                          value="{{ $order->name }}"
                                                                                          class="form-control"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2 control-label">Email<strong
                                                                        class="required">*</strong></div>
                                                            <div class="col-sm-10"><input name="email"
                                                                                          placeholder="Email"
                                                                                          value="{{ $order->email }}"
                                                                                          class="form-control"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2 control-label">Số điện thoại</div>
                                                            <div class="col-sm-10"><input name="phone"
                                                                                          placeholder="Số điện thoại"
                                                                                          value="{{ $order->phone }}"
                                                                                          class="form-control"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2 control-label">Địa chỉ</div>
                                                            <div class="col-sm-10"><input name="address"
                                                                                          placeholder="Địa chỉ"
                                                                                          value="{{ $order->address }}"
                                                                                          class="form-control"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @php
                                    $counter = 0;
                                @endphp
                                <div id="tab-order" class="tab-pane fade">
                                    <div class="box box-table">
                                        <div class="box-body">
                                            <table cellspacing="0" width="100%"
                                                   class="table table-condensed table-bordered table-striped display nowrap dataTable"
                                                   id="DataTables_Table_0">
                                                <thead></thead>
                                                <thead>
                                                <tr>
                                                    <th>Mã đơn hàng</th>
                                                    <th>Ngày đặt hàng</th>
                                                    <th>Trạng thái</th>
                                                    <th>Tổng tiền</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($orders as $order)
                                                <tr>
                                                    <td><a href="/admin/order?id={{$order->id}}" target="_blank">#{{$order->id}}</a></td>
                                                    <td>{{$order->created_at}}</td>
                                                    <td>
                                                        @if ($order->order_status == 'new')
                                                            <label class="label label-success">Mới</label>
                                                        @elseif ($order->order_status == 'confirmed')
                                                            <label class="label label-default">Đã xác nhận</label>
                                                        @elseif ($order->order_status == 'paid')
                                                            <label class="label label-primary">Đã thanh toán</label>
                                                        @elseif ($order->order_status == 'cancel')
                                                            <label class="label label-danger">Bị hủy</label>
                                                        @endif
                                                    </td>
                                                    <td>{{Helper::formatMoney($order->total)}} VNĐ</td>
                                                </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="col-xs-12 col-sm-12 col-md-4 block">--}}
            {{--<div class="box">--}}
                {{--<div class="form-horizontal">--}}
                    {{--<div class="box-title clearfix">--}}
                        {{--<div class="col-xs-12 title">Trạng thái đơn hàng</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="tab-content">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Mã đơn hàng:</div>--}}
                                {{--<div class="col-sm-6">{{$order->id}}</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Phương thức thanh toán:</div>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<label class="label label-info">{{$order->payment_method}}</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Trạng thái:</div>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<select class="" name="order_status" class="form-control" data-id="{{$order->id}}">--}}
                                        {{--@php--}}
                                            {{--$status = ['new', 'confirmed', 'paid', 'cancel'];--}}
                                            {{--$title = ['Mới', 'Đã xác nhận', 'Đã thanh toán', 'Đã hủy'];--}}
                                        {{--@endphp--}}
                                        {{--@for ($i=0; $i < 4; $i++)--}}
                                            {{--<option value="{{$status[$i]}}"--}}
                                                    {{--@if ($order->order_status == $status[$i])--}}
                                                    {{--selected="selected"--}}
                                                    {{--@endif--}}
                                            {{-->{{$title[$i]}}</option>--}}
                                        {{--@endfor--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Ngày tạo:</div>--}}
                                {{--<div class="col-sm-6"><strong>{{$order->created_at}}</strong></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-8 block">--}}
            {{--<div class="box">--}}
                {{--<div class="form-horizontal">--}}
                    {{--<div class="box-title clearfix">--}}
                        {{--<div class="col-xs-12 title">THÔNG TIN KHÁCH HÀNG--}}
                            {{--<span><a data-id="{{ $order->id }}" data-type="customer" title="Chỉnh sửa"--}}
                                     {{--class="c-btn-edit btn-edit-information_customer"><i--}}
                                            {{--class="fa fa-pencil-square-o"></i></a></span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="tab-content">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Họ tên:</div>--}}
                                {{--<div class="col-sm-6">{{$order->customer->name}}</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Email:</div>--}}
                                {{--<div class="col-sm-6">{{$order->customer->email}}</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Điện thoại:</div>--}}
                                {{--<div class="col-sm-6">{{$order->customer->phone}}</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Địa chỉ:</div>--}}
                                {{--<div class="col-sm-6">{{$order->customer->address}}</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-8 block">--}}
            {{--<div class="box">--}}
                {{--<div class="form-horizontal">--}}
                    {{--<div class="box-title clearfix">--}}
                        {{--<div class="col-xs-12 title">THÔNG TIN GIAO HÀNG--}}
                            {{--<span><a data-id="{{ $order->id }}" data-type="shipping" title="Chỉnh sửa"--}}
                                     {{--class="c-btn-edit btn-edit-information_shipping"><i--}}
                                            {{--class="fa fa-pencil-square-o"></i></a></span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="tab-content">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Tên người nhận:</div>--}}
                                {{--<div class="col-sm-6">{{$order->receiver_name}}</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Điện thoại người nhận:</div>--}}
                                {{--<div class="col-sm-6">{{$order->receiver_phone}}</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-6 text-left">Địa chỉ nhận hàng:</div>--}}
                                {{--<div class="col-sm-6">{{$order->receiver_address}}</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div id="modal-edit-customer_customer" role="dialog" class="modal fade" style="display: none;">--}}
            {{--<div class="modal-dialog">--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-header">--}}
                        {{--<button type="button" data-dismiss="modal" class="close">×</button>--}}
                        {{--<div class="modal-title"><h4 class="modal-title">Chỉnh sửa thông tin<span class="name-type">--}}
                                    {{--giao hàng</span></h4></div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-body">--}}
                        {{--<div class="form-horizontal">--}}
                            {{--<div class="box-body">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-2 control-label">Họ tên<strong class="required">*</strong></div>--}}
                                    {{--<div class="col-sm-10"><input name="name" placeholder="Họ tên"--}}
                                                                  {{--value="{{$order->customer->name}}"--}}
                                                                  {{--class="form-control"></div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-2 control-label">Email<strong class="required">*</strong></div>--}}
                                    {{--<div class="col-sm-10"><input name="email" placeholder="Email"--}}
                                                                  {{--value="{{$order->customer->email}}"--}}
                                                                  {{--class="form-control inputmark-email"></div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-2 control-label">Số điện thoại</div>--}}
                                    {{--<div class="col-sm-10"><input name="phone" type="number" placeholder="Số điện thoại"--}}
                                                                  {{--value="{{$order->customer->phone}}"--}}
                                                                  {{--class="form-control inputmark-phone"></div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-2 control-label">Địa chỉ</div>--}}
                                    {{--<div class="col-sm-10"><input name="address" placeholder="Địa chỉ"--}}
                                                                  {{--value="{{$order->customer->address}}"--}}
                                                                  {{--class="form-control"></div>--}}
                                {{--</div>--}}
                                {{--<div class="order_fee hidden"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" data-dismiss="modal" class="btn btn-default">Hủy</button>--}}
                        {{--<button class="btn btn-primary btn-update" data-type="customer"--}}
                                {{--data-id="{{ $order->customer_id }}">Cập nhật--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div id="modal-edit-customer_shipping" role="dialog" class="modal fade" style="display: none;">--}}
            {{--<div class="modal-dialog">--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-header">--}}
                        {{--<button type="button" data-dismiss="modal" class="close">×</button>--}}
                        {{--<div class="modal-title"><h4 class="modal-title">Chỉnh sửa thông tin<span class="name-type">--}}
                                    {{--giao hàng</span></h4></div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-body">--}}
                        {{--<div class="form-horizontal">--}}
                            {{--<div class="box-body">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-2 control-label">Họ tên<strong class="required">*</strong></div>--}}
                                    {{--<div class="col-sm-10"><input name="receiver_name" placeholder="Họ tên"--}}
                                                                  {{--value="{{$order->receiver_name}}"--}}
                                                                  {{--class="form-control"></div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-2 control-label">Số điện thoại</div>--}}
                                    {{--<div class="col-sm-10"><input name="receiver_phone" type="number"--}}
                                                                  {{--placeholder="Số điện thoại"--}}
                                                                  {{--value="{{$order->receiver_phone}}"--}}
                                                                  {{--class="form-control inputmark-phone"></div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-2 control-label">Địa chỉ</div>--}}
                                    {{--<div class="col-sm-10"><input name="receiver_address" placeholder="Địa chỉ"--}}
                                                                  {{--value="{{$order->receiver_address}}"--}}
                                                                  {{--class="form-control"></div>--}}
                                {{--</div>--}}
                                {{--<div class="order_fee hidden"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" data-dismiss="modal" class="btn btn-default">Hủy</button>--}}
                        {{--<button class="btn btn-primary btn-update" data-type="shipping" data-id="{{ $order->id }}">Cập--}}
                            {{--nhật--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    <script type="text/javascript">
        $('.btn-edit-information_customer').on('click', function () {
            var modal = $('#modal-edit-customer_customer');
            modal.modal('show');

        })
        $('#modal-edit-customer_customer .btn-update').click(function () {
            var id = $(this).data('id');
            var type = $(this).data('type');
            $('input').removeClass('error');
            var data = {};
            data.id = id;
            data.name = $('input[name="name"]').val();
            data.phone = $('input[name="phone"]').val();
            data.email = $('input[name="email"]').val();
            data.address = $('input[name="address"]').val();

            if (!data.name) {
                toastr.error('Chưa nhập họ tên khách hàng');
                $('input[name="name"]').addClass('error');
                return;
            }
            if (!data.email) {
                toastr.error('Chưa nhập email khách hàng');
                $('input[name="email"]').addClass('error');
                return;
            }

            if (!data.phone) {
                toastr.error('Chưa nhập số điện thoại khách hàng');
                $('input[name="phone"]').addClass('error');
                return;
            }
            if (!data.address) {
                toastr.error('Chưa nhập địa chỉ khách hàng');
                $('input[name="address"]').addClass('error');
                return;
            }
            if (type == 'customer') {
                updateCustomer(data);
            }
        })

        function updateCustomer(data) {
            $.ajax({
                type: 'POST',
                url: '/admin/customer/updateInfo',
                data: data,
                success: function (json) {
                    $(document).find('.disabled').removeClass('disabled');
                    if (!json.code) {
                        console.dir(json);
                        $('#modal-edit-customer_customer').modal('hide');
                        toastr.success('Cập nhật thành công');
                        window.location.reload();
                    }
                }
            });
        }

        $('.btn-edit-information_shipping').on('click', function () {
            var modal = $('#modal-edit-customer_shipping');
            modal.modal('show');
        })
        $('#modal-edit-customer_shipping .btn-update').click(function () {
            var id = $(this).data('id');
            var type = $(this).data('type');
            $('input').removeClass('error');
            var data = {};
            data.id = id;
            data.receiver_name = $('input[name="receiver_name"]').val();
            data.receiver_phone = $('input[name="receiver_phone"]').val();
            data.receiver_address = $('input[name="receiver_address"]').val();

            if (!data.receiver_phone) {
                toastr.error('Chưa nhập họ tên người nhận');
                $('input[name="receiver_name"]').addClass('error');
                return;
            }
            if (!data.receiver_phone) {
                toastr.error('Chưa nhập số điện thoại người nhận');
                $('input[name="receiver_phone"]').addClass('error');
                return;
            }
            if (!data.receiver_address) {
                toastr.error('Chưa nhập địa chỉ người nhận');
                $('input[name="receiver_address"]').addClass('error');
                return;
            }
            if (type == 'shipping') {
                updateShipping(data);
            }
        })

        function updateShipping(data) {
            $.ajax({
                type: 'POST',
                url: '/admin/order/updateInfo',
                data: data,
                success: function (json) {
                    $(document).find('.disabled').removeClass('disabled');
                    if (!json.code) {
                        console.dir(json);
                        $('#modal-edit-customer_shipping').modal('hide');
                        toastr.success('Cập nhật thành công');
                        window.location.reload();
                    }
                }
            });
        }

        $('.btn-edit-information_shipping').on('click', function () {
            var modal = $('#btn-edit-information_shipping');
            modal.modal('show');
        })
        $("[data-toggle=tooltip]").tooltip();
        $('select[name=order_status]').on('change', function () {
            var id = $(this).data('id');
            var status = $(this).val();
            var params = {
                type: 'POST',
                url: '/admin/changeStatusOrder',
                data: {ids: [id], status: status},
                success: function (result) {
                    result = $.parseJSON(result);
                    if (result.code == 0) {
                        alert("Thành công!")
                    }
                }
            }
            $.ajax(params);
        })
    </script>
@endsection
