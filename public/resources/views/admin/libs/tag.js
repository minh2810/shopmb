$('.list-tags').on('click', '.tag-name', function () {
  var text = $(this).html();
  $("input[name='tags']").tagsinput('add', text);
});
$('.list-authors').on('click', '.author-name', function () {
  var text = $(this).html();
  $("input[name='author']").tagsinput('removeAll');
  $("input[name='author']").tagsinput('add', text);
});
