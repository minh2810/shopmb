function initImage() {
  $.each($('.image-preview'), function(i, e) {
    var params = {
      type: 'POST',
      url: '/admin/getImage',
      data: {
        key: $(e).data('key'),
        type: $(e).data('type')
      },
      success: function(result) {
        result = $.parseJSON(result);
        if (result.code == 0 && result.data != "") {
          $(e).css("background", "url('/uploads/" + result.data + "')");
          $(e).css("background-size", "cover");
          $(e).css("background-position", "center center");
        }
      }
    }
    $.ajax(params);
  })
}
function money(num) {
  if (num) num = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  return num;
}

$(document).on('keyup', '.formatMoney',function() {
    strToPrice($(this).val());
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
        return;
    }
    // When the arrow keys are pressed, abort.
    if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
        return;
    }
    var $this = $( this );

    // Get the value.
    var input = $this.val();

    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $this.val( function() {
        return input;
    } );
});

// Sanitize the values.
function strToPrice(price){
    if (price)  {
        price = price.replace(/,/g, '');
        return parseInt(price);
    }
    return price;
}
//numver to String format money
function numToStringPrice(num){
    if(num) return  num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}
