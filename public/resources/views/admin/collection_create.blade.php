@extends('admin.layout')
@section('header')     <li class="hidden-xs"><a href="/admin/collection_all">Nhóm sản phẩm</a></li>
<li class="title-ellipsis"><a>Thêm mới</a></li>
@endsection
@section('button_save')
  <ol class="button-right col-md-4 col-sm-5 col-xs-12">
    <li><a class="btn btn-admin btn-create btn_create_collection"><i class="fa fa-floppy-o"></i><i
                class="fa fa-circle-o-notch fa-spin"></i>Lưu lại</a></li>
  </ol>
@endsection
@section('content')
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="box box-info">
      <div class="form-horizontal form-product">
        <div class="box-title clearfix">
          <div class="pull-left title-tab">Tạo nhóm sản phẩm
            <div class="action_result">
              <span style="color: red;"></span>
            </div>
          </div>
        </div>
        <div class="box-body">
          <div class="box-body">
            <div class="tab-content">
              <div class="form-group">
                <div class="col-lg-2 col-md-12 control-label">Tiêu đề<strong class="required">*</strong></div>
                <div class="col-lg-10 col-md-12">
                  <input name="name" placeholder="Tiêu đề" class="form-control title text-overflow-title">
                </div>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="tab-content">
              <div class="form-group">
                <div class="col-lg-2 col-md-12 control-label">Mô tả<strong class="required">*</strong></div>
                <div class="col-lg-10 col-md-12">
                  <textarea name="description" rows="8" cols="80" placeholder="Mô tả" class="form-control title text-overflow-title"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="tab-content">
              <div class="form-group">
                <div class="col-lg-2 col-md-12 control-label">Nội dung<strong class="required">*</strong></div>
                <div class="col-lg-10 col-md-12">
                  <textarea id="collection_content" name="content" rows="8" cols="80" placeholder="Mô tả" class="tinymce form-control title text-overflow-title"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="tab-content">
              <div class="form-group">
                <div class="col-lg-2 col-md-12 control-label">Chọn sản phẩm</div>
                <div class="col-lg-10 col-md-12">
                  <select multiple="multiple" class="multiple-select settings" name="product_ids">
                      @foreach (Helper::getAllProduct() as $product)
                          <option value="{{$product->id}}">{{$product->title}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="tab-content">
              <div class="form-group">
                  <div class="col-lg-2 col-md-12 control-label">Hình đại diện<strong class="required">*</strong></div>
                  <div class="col-lg-4 col-md-12">
                    <div class="image-preview" name="image">
                      <label for="image-upload" class="image-label">Chọn hình</label>
                      <input type="file" name="image" class="image-upload"/>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-12">Trạng thái:<br>
                    <input type="radio" name="status" value="active" checked="checked"><span class="label label-success">Sẵn sàng</span><br>
                    <input type="radio" name="status" value="deleted"><span class="label label-danger">Bị xóa</span><br>
                    <input type="radio" name="status" value="hiden"><span class="label label-warning">Bị ẩn</span><br>
                  </div>
              </div>
            </div>
          </div>
          {{--<div class="box-body">--}}
            {{--<div class="tab-content">--}}
              {{--<div class="form-group">--}}
                {{--<div class="col-lg-4 col-md-4 pull-right">--}}
                  {{--<input type="button" id="collection-create" value="Tạo" class="form-control title text-overflow-title btn btn-primary">--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
tinymce.init({
  selector: 'textarea.tinymce',
  plugins: 'codesample'
});
$(function () {
    $('.multiple-select').multipleSelect({
        width: '100%'
    });
});
$(document).ready(function() {
  $.uploadPreview({
    input_field: ".image-upload", // Default: .image-upload
    preview_box: ".image-preview", // Default: .image-preview
    label_field: ".image-label", // Default: .image-label
    label_default: "Chọn hình", // Default: Choose File
    label_selected: "", // Default: Change File
    no_label: true
  });
  $('.btn_create_collection').on('click', function() {
    try {
      var name = $('input[name=name]').val();
      var image = $('input[name=image]').prop('files')[0].name;
      var description = $('textarea[name=description]').val();
      var content = tinymce.get('collection_content').getContent();
      var status = $('input[name=status]:checked').val();
      var product_ids = $('select[name=product_ids]').val();
    } catch (e) {
      $('.action_result span').text('Hãy nhập đầy đủ thông tin');
      window.scrollTo(document.body.scrollWidth, 0);
      return;
    }
    if (name == "" || description == "" || content == "" || status == "") {
      $('.action_result span').text('Hãy nhập đầy đủ thông tin');
      window.scrollTo(document.body.scrollWidth, 0);
      return;
    }
    var formdata = new FormData();
    var file = $('input[name=image]').prop('files')[0];
    formdata.append('image', file);
    var params1 = {
      url: '/admin/uploadImage',
      type: 'POST',
      data: formdata,
      processData: false,
      contentType: false,
      success: function(result1) {
        var image = $.parseJSON(result1).image_name;
        var params = {
          url: '/admin/createCollection',
          type: 'POST',
          data: {
            name: name,
            image: image,
            description: description,
            content: content,
            status: status,
            product_ids: product_ids
          },
          success: function(result) {
            alert('Thêm thành công nhóm sản phẩm');
            window.location.href = '/admin/collection_all';
          }
        }
        $.ajax(params);
      }
    }
    $.ajax(params1);
  })
})
</script>
@endsection
