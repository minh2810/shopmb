@extends('admin.layout')
@section('header')  Liên hệ
@endsection
@section('content')
    <section class="clearfix">
        <div class="box box-table">
            <div class="row box-header title-tab"><h3 class="box-title">DANH SÁCH</h3></div>
            <div class="box-body">
                <div class="action-box hidden">
                    <div class="dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><i
                                    class="fa fa-share"></i> Chọn thao tác (đang chọn<span class="num-select"></span>
                            liên hệ)  <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a data-value="read" class="contact-status">Đã đọc</a></li>
                            <li><a data-value="unreply" class="contact-status">Chưa phản hồi</a></li>
                            <li><a data-value="reply" class="contact-status">Đã phản hồi</a></li>
                            <li><a data-value="delete" class="contact-status">Xóa</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="dataTables_length" id="DataTables_Table_0_length"><label>Hiển
                            thị
                            <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                                    class="form-control input-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> dữ liệu</label></div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Tìm
                            kiếm:
                            <input name="search" class="form-control input-sm" placeholder="Nhập từ khóa cần tìm..."
                                   aria-controls="DataTables_Table_0"></label></div>
                </div>
                <div class="box-body">
                    <div class="tab-content">
                        <div class="form-group">
                            <div class="col-lg-2 col-md-6">
                                <strong>Chỉ hiện</strong>
                            </div>
                            <div class="col-lg-10 col-md-6">
                                <a href="/admin/contact_all?sort=only-Read"><span
                                            class="label label-warning">Read</span> </a>|
                                <a href="/admin/contact_all?sort=only-Unread"><span
                                            class="label label-success">Unread</span> </a>|
                                <a href="/admin/contact_all?sort=only-Delete"><span
                                            class="label label-danger">Delete</span> </a>|
                                <a href="/admin/contact_all"><span class="label label-info">All</span>
                                </a>
                            </div>
                        </div>
                        <div class="form-group" id="actions" style="display: none">
                            <div class="col-lg-2 col-md-6">
                                <strong>Chuyển tất cả thành </strong>
                            </div>
                            <div class="col-lg-10 col-md-6">
                                <a href="/admin/contact_all?sort=only-Read"><span
                                            class="label label-warning">Read</span> </a>|
                                <a href="/admin/contact_all?sort=only-Unread"><span
                                            class="label label-success">UnRead</span> </a>|
                                <a href="/admin/contact_all?sort=only-hiden"><span
                                            class="label label-danger">Deleted</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                            <th><input type="checkbox" id="checkall"/></th>
                            <th>#id</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>
                            <th>Ngày cập nhật</th>
                            <th>Trạng thái</th>
                            </thead>
                            <tbody>
                            @foreach ($contacts as $contact)
                                <tr class="item" role="row" id="{{ $contact->id }}">
                                    <td><input type="checkbox" value="{{ $contact->id }}"
                                               class="checkthis action"/></td>
                                    <td><a href="/admin/contact?id={{ $contact->id }}">#{{ $contact->id }}</a></td>
                                    <td class="all"><a
                                                href="/admin/contact?id={{ $contact->id }}">{{ $contact->name }}</a>
                                    </td>
                                    <td class="hidden">{{ $contact->name }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->phone }}</td>
                                    <td>{{ $contact->created_at }}</td>
                                    <td>                     @if ($contact->status == 'Read')
                                            <label class="label label-warning">{{ $contact->status }}</label>
                                        @elseif ($contact->status == 'Unread')
                                            <label class="label label-success">{{ $contact->status }}</label>
                                        @elseif ($contact->status == 'Delete')
                                            <label class="label label-danger">{{ $contact->status }}</label>
                                        @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <ul class="pagination pull-right">
                            <li class="disabled"><a href="#"><span
                                            class="glyphicon glyphicon-chevron-left"></span></a></li>
                            @for  ($i=1; $i <= $total_page; $i++)
                                <li class="
                  @if ($page == $i)
                                        active
@endif
                                        "><a
                                            href="/admin/product_all?page={{$i}}&perpage={{$perpage}}">{{$i}}</a>
                                </li>
                            @endfor
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script type="text/javascript">
        $("#mytable #checkall").click(function () {
            if ($("#mytable #checkall").is(':checked')) {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", true);
                });
            } else {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
        $("[data-toggle=tooltip]").tooltip();
        $('input.action, #checkall').on('click', function (e) {
            var action = false;
            $.each($('input.action'), function (i, e) {
                if ($(e).prop('checked')) {
                    action = true;
                    return;
                }
            })
            if (action) {
                $('#actions').css('display', 'block');
            } else {
                $('#actions').css('display', 'none');
            }
        })
        $('input[name=search]').on('keyup', function () {
            var search = $(this).val();
            $.ajax({
                url: '/admin/contact/search', type: 'POST', data: {search: search}, success: function (result) {
                    $('.item').remove();
                    result = $.parseJSON(result);
                    $.each(result.result, function (i, e) {
                        var html = '<tr class="item" role="row" id="' + e.id + '">' +
                        '<td><input type="checkbox" value="'+ e.id +'" class="checkthis action"></td>' +
                        '<td><a href="/admin/contact?id='+ e.id +'">' + '#' + e.id + '</a></td>'    +
                        '<td class="all"><a href="/admin/contact?id=' + e.id + '">' + e.name + '</a></td>' +
                        '<td>'+ e.email + '</td>' +
                        '<td>'+ e.phone + '</td>' +
                        '<td>'+ e.updated_at + '</td>' +
                        '<td>' ;
                        if(e.status == 'Delete') {
                                 html += '<label class="label label-danger">'+
                                 e.status +
                                 '</label>'
                        } else if(e.status == 'Read') {
                            html += '<label class="label label-warning">'+
                                e.status  +
                                '</label>'
                        } else if(e.status == 'Unread') {
                            html += '<label class="label label-success">'+
                                e.status  +
                                '</label>'
                        }
                        html += '</td>' +
                        '                    </tr>  ';
                        $('#mytable tbody').append(html);
                    });
                }
            });
        });
    </script>
@endsection
