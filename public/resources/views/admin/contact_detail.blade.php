@extends('admin.layout')
@section('header')  Liên hệ
@endsection
@section('content')
    <section class="clearfix">
        <div class="box box-table">
            <div class="box-body">
                <div class="pull-left"><p><b>Trạng thái: </b>
                        @if ($contact->status == 'Read')
                        <label class="label label-warning">{{ $contact->status }}</label>
                        @elseif ($contact->status == 'Unread')
                        <label class="label label-success">{{ $contact->status }}</label>
                            @elseif ($contact->status == 'Delete')
                        <label class="label label-danger">{{ $contact->status }}</label>
                            @endif
                    </p>
                    <p><b>Thời gian: </b>{{ $contact->created_at }}</p>
                    <p><b>Họ tên: </b>{{ $contact->name }}</p>
                    <p><b>Email: </b><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></p>
                    <p><b>Điện thoại: </b><a href="tel:{{ $contact->phone }}">{{ $contact->phone }}</a></p></div>
                <div class="pull-right">
                    <div class="action-contact">
                        <div class="dropdown">
                            <div class="col-md-12 pull-right">
                                <select class="" name="contact_status" class="form-control" data-id="{{$contact->id}}">
                                    @php
                                        $status = ['Unread', 'Read', 'Delete'];
                                        $title = ['Chưa phản hồi', 'Đã phản hồi', 'Đã xóa'];
                                    @endphp
                                    @for ($i=0; $i < 3; $i++)
                                        <option value="{{$status[$i]}}"
                                                @if ($contact->status == $status[$i])
                                                selected="selected"
                                                @endif>{{$title[$i]}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>
                <p><b>Nội dung:</b></p>
                <p>{{ $contact->content }}</p></div>
        </div>
    </section>

    <script type="text/javascript">
        $('select[name=contact_status]').on('change', function () {
            var id = $(this).data('id');
            var status = $(this).val();
            var params = {
                type: 'POST',
                url: '/admin/changeStatusContact',
                data: {ids: [id], status: status},
                success: function (result) {
                    result = $.parseJSON(result);
                    if (result.code == 0) {
                        alert("Thành công!")
                        setTimeout(function () {
                            window.location.href = '/admin/contact_all';
                        }, 500);
                    }
                }
            }
            $.ajax(params);
        })
    </script>
@endsection
