@extends('master')
@section('content')
<div class="breadcrumb-wrapper">
  <div class="breadcrumb-title">
    <h1 class="page-title"><span>Thanh Toán</span></h1>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="/">Trang chủ</a></li>
        <li><a href="cart">Giỏ Hàng</a></li>
      </ul>
    </div>

  </div>
</div>
<div class="container">
  <div class="row">
    <div id="content" class="col-sm-12">
      <div class="position-display">
      </div>
      <h1>Giỏ Hàng</h1>
      @if (isset($message))
      <h3>{{$message}}</h3>
      @else
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="text-center">Hình ảnh</td>
              <td class="text-left">Tên sản phẩm</td>
              <td class="text-left">Dòng sản phẩm</td>
              <td class="text-left">Số lượng</td>
              <td class="text-right">Đơn giá</td>
              <td class="text-right">Tổng cộng</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($cart->items as $item)
            <tr>
              <td class="text-center"> <a href="product?id={{$item->id}}"><img style="width: 55px; height: 55px;" src="uploads/{{$item->featured_image}}" alt="{{$item->title}}" title="{{$item->title}}" class="img-thumbnail"></a>
              </td>
              <td class="text-left"><a href="product?id={{$item->id}}">{{$item->title}}</a>
              </td>
              <td class="text-left">{{$item->id}}</td>
              <td class="text-left">
                <div class="input-group btn-block" style="max-width: 200px;">
                  <input type="text" name="quantity" data-id="{{$item->id}}" data-quantity="{{$item->quantity}}" value="{{$item->quantity}}" size="1" class="form-control qty">
                  <span class="input-group-btn">
        <button type="submit" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Cập nhật"><i class="fa fa-refresh"></i></button>
        <button type="button" data-toggle="tooltip" title="" data-id="{{$item->id}}" class="btn btn-danger btn-remove" data-original-title="Loại bỏ"><i class="fa fa-times-circle"></i></button></span></div>
              </td>
              <td class="text-right">{{Helper::formatMoney($item->price)}} VNĐ</td>
              <td class="text-right">{{Helper::formatMoney($item->price * $item->quantity)}} VNĐ</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <h2>Bạn muốn làm gì tiếp theo?</h2>
      <p>Sử dụng Voucher để nhận ưu đãi:</p>
      <div class="panel-group" id="accordion">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><a href="#collapse-voucher" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" aria-expanded="false">Sử dụng Phiếu Voucher <i class="fa fa-caret-down"></i></a></h4>
          </div>
          <div id="collapse-voucher" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
              <label class="col-sm-2 control-label" for="input-voucher">Nhập mã Phiếu Voucher</label>
              <div class="input-group">
                <input type="text" name="voucher" value="" placeholder="Nhập mã Phiếu Voucher" id="input-voucher" class="form-control">
                <span class="input-group-btn">
        <input type="submit" value="Xác nhận" id="button-voucher" data-loading-text="Đang Xử lý..." class="btn btn-primary">
        </span> </div>
              <script type="text/javascript">
                <!--
                $('#button-voucher').on('click', function() {
                  $.ajax({
                    url: 'index.php?route=checkout/voucher/voucher',
                    type: 'post',
                    data: 'voucher=' + encodeURIComponent($('input[name=\'voucher\']').val()),
                    dataType: 'json',
                    beforeSend: function() {
                      $('#button-voucher').button('loading');
                    },
                    complete: function() {
                      $('#button-voucher').button('reset');
                    },
                    success: function(json) {
                      $('.alert').remove();

                      if (json['error']) {
                        $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        $('html, body').animate({
                          scrollTop: 0
                        }, 'slow');
                      }

                      if (json['redirect']) {
                        location = json['redirect'];
                      }
                    }
                  });
                });
                //-->
              </script>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-sm-4 col-sm-offset-8">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td class="text-right"><strong>Thành tiền:</strong></td>
                <td class="text-right">{{Helper::formatMoney($cart->total)}} VNĐ</td>
              </tr>
              <tr>
                <td class="text-right"><strong>Thuế(Cố định):</strong></td>
                <td class="text-right">20.000 VNĐ</td>
              </tr>
              <tr>
                <td class="text-right"><strong>Tổng cộng :</strong></td>
                <td class="text-right">{{Helper::formatMoney($cart->total + 20000)}} VNĐ</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="buttons">
        <div class="pull-left"><a href="/" class="btn btn-default">Tiếp tục mua hàng</a></div>
        <div class="pull-right"><a href="checkout" class="btn btn-primary">Thanh toán</a></div>
      </div>
      <div class="position-display">
      </div>
    </div>
    @endif
  </div>
</div>
<script type="text/javascript">
  $('.btn-remove').on('click', function(e) {
    var main = $(this);
    e.preventDefault();
    var id = $(this).data('id');
    StoreAPI.removeItem(id, function(result) {
        window.location.reload();
      main.parent().parent().parent().parent().empty();
      alert("Xóa thành công!");
    })
  })
  $('input[name=quantity]').on('change', function(e) {
    e.preventDefault();
    var main = $(this);
    var id = $(this).data('id');
    var quantity = $(this).val();
    var sum = 0;
    StoreAPI.changeItem(id, quantity, function(result) {
      main.parent().parent().next().text(money(result.data.price) + " VNĐ");
      main.parent().parent().next().next().text(money(result.data.price * result.data.quantity) + " VNĐ");
        $('.qty').each(function() {
            sum += +$(this).val();
        })
        $('.num_product').html(parseInt(sum));
    })
  })
</script>
@endsection
