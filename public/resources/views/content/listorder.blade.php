@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Lịch Sử Đặt Hàng</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="/customer">Tài khoản</a></li>
                    <li class="active"><a href="">Lịch Sử Đặt
                            Hàng</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @php
                $orders = Helper::getOrderByCustomerID($customer->id)
            @endphp
            @if(count($orders) == 0)
                <div id="content" class="col-sm-12">
                    <div class="position-display">
                    </div>
                    <p>Bạn chưa có đơn hàng nào!</p>
                    <div class="buttons clearfix button-box">
                        <div class="pull-right">
                            <a href="/" class="btn btn-primary">Tiếp tục</a>
                        </div>
                    </div>
                    <div class="position-display">
                    </div>
                </div>
            @else
                <div id="content" class="col-sm-12">
                    <div class="position-display">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-right">Mã đơn hàng</td>
                                <td class="text-right">Tình trạng</td>
                                <td class="text-right">Ngày tạo</td>
                                <td class="text-right">Số lượng Đặt mua</td>
                                <td class="text-right">Khách Hàng</td>
                                <td class="text-right">Tổng Cộng</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td class="text-right">#{{ $order->id }}</td>
                                    <td class="text-right">{{ $order->order_status }}</td>
                                    <td class="text-right">{{ $order->created_at }}</td>
                                    @php
                                        $order_details = Helper::getOrderDetail($order->id)
                                    @endphp
                                    <td class="text-right">
                                        @foreach($order_details as $order_detail)
                                              {{ $order_detail->quantity }}
                                        @endforeach
                                    </td>
                                    @php
                                        $customer_infos = Helper::getInfoCustomerById($customer->id);
                                    @endphp
                                    <td class="text-right">
                                        @foreach($customer_infos as $customer_info )
                                            {{ $customer_info->name }}
                                        @endforeach
                                    </td>
                                    <td class="text-right">{{Helper::formatMoney($order->total)}} VNĐ</td>
                                    <td class="text-right"><a
                                                href="/detailOrder?id={{ $order->id }}"
                                                data-toggle="tooltip" title="" class="btn btn-info"
                                                data-original-title="Xem"><i
                                                    class="fa fa-eye"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right"></div>
                    <div class="buttons clearfix button-box">
                        <div class="pull-right"><a href="/"
                                                   class="btn btn-primary">Tiếp tục</a></div>
                    </div>
                    <div class="position-display">
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection