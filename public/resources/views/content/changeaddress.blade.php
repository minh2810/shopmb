@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Địa Chỉ</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="/customer">Tài khoản</a></li>
                    <li class="active"><a href="#">Địa Chỉ</a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display">
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td class="text-left">
                                {{ $customer->address }}
                            </td>
                            <td class="text-right"><a
                                        href="http://9736.chilishop.net/index.php?route=account/address/edit&amp;address_id=5"
                                        class="btn btn-info">Sửa</a> &nbsp;
                                <a
                                        data-id="{{$customer->id}}"
                                        href=""
                                        class="btn btn-danger">Xóa</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons clearfix button-box">
                    <div class="pull-left"><a href="/customer"
                                              class="btn btn-default">Quay lại</a></div>
                    <div class="pull-right"><a href="/newaddress"
                                               class="btn btn-primary">Địa chỉ mới</a></div>
                </div>
                <div class="position-display">
                </div>
            </div>
        </div>
    </div>
@endsection
