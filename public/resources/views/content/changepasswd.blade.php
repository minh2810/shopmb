@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Thay đổi mật khẩu</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="/customer">Tài khoản</a></li>
                    <li class="active"><a href="#">Thay đổi mật
                            khẩu</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display">
                </div>
                @if (!empty($action_result))
                    <h3><strong style="color: red;">{{$action_result}}</strong></h3>
                @endif
                <form action="changePasswd" method="post"
                      enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <fieldset>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-password">Mật Khẩu:</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" value="" placeholder="Mật Khẩu:"
                                       id="input-password"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-confirm">Nhập lại mật khẩu:</label>
                            <div class="col-sm-10">
                                <input type="password" name="confirm" value="" placeholder="Nhập lại mật khẩu:"
                                       id="input-confirm" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <div class="buttons clearfix button-box">
                        <div class="pull-left"><a href="/customer"
                                                  class="btn btn-default">Quay lại</a></div>
                        <div class="pull-right">
                            <input type="submit" value="Tiếp tục" class="btn btn-primary">
                        </div>
                    </div>
                </form>
                <div class="position-display">
                </div>
            </div>
        </div>
    </div>
@endsection