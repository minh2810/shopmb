@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Thông tin đơn hàng</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="/customer">Tài khoản</a></li>
                    <li><a href="/listOrder">Lịch Sử Đặt Hàng</a></li>
                    <li class="active"><a
                                href="#">Thông
                            tin đơn hàng</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display">
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left" colspan="2">Chi tiết đơn hàng</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left" style="width: 50%;"><b>Mã đơn hàng:</b> #42<br>
                                <b>Ngày tạo:</b> 30/09/2018
                            </td>
                            <td class="text-left"><b>Phương thức thanh toán:</b> Thu tiền khi giao hàng<br>
                                <b>Phương thức vận chuyển:</b> Phí vận chuyển cố định
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left" style="width: 50%;">Địa chỉ thanh toán</td>
                            <td class="text-left">Địa chỉ giao hàng</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left">nguyen nguyen<br>minh<br>234 dong bac<br>phuong tan chanh hiep quan 12<br>An
                                Giang<br>Việt Nam
                            </td>
                            <td class="text-left">nguyen nguyen<br>minh<br>234 dong bac<br>phuong tan chanh hiep quan 12<br>An
                                Giang<br>Việt Nam
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left">Tên sản phẩm</td>
                            <td class="text-left">Dòng sản phẩm</td>
                            <td class="text-right">Số lượng</td>
                            <td class="text-right">Đơn Giá</td>
                            <td class="text-right">Tổng Cộng</td>
                            <td style="width: 20px;"></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left">Thỏ bông thân thiện <br>
                                &nbsp;<small> - Kích thước: Trung bình</small>
                            </td>
                            <td class="text-left">M003</td>
                            <td class="text-right">1</td>
                            <td class="text-right">320.000 VNĐ</td>
                            <td class="text-right">320.000 VNĐ</td>
                            <td class="text-right" style="white-space: nowrap;"><a
                                        href="http://9736.chilishop.net/index.php?route=account/order/reorder&amp;order_id=42&amp;order_product_id=3790"
                                        data-toggle="tooltip" title="" class="btn btn-primary"
                                        data-original-title="Đặt hàng lại"><i class="fa fa-shopping-cart"></i></a>
                                <a href="http://9736.chilishop.net/index.php?route=account/return/add&amp;order_id=42&amp;product_id=3"
                                   data-toggle="tooltip" title="" class="btn btn-danger"
                                   data-original-title="Đổi / Trả Hàng"><i class="fa fa-reply"></i></a></td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right"><b>Thành tiền</b></td>
                            <td class="text-right">320.000 VNĐ</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right"><b>Điểm thưởng():</b></td>
                            <td class="text-right">0 VNĐ</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right"><b>Phí vận chuyển cố định</b></td>
                            <td class="text-right">20.000 VNĐ</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right"><b>Sản phẩm tính thuế</b></td>
                            <td class="text-right">2.000 VNĐ</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right"><b>Tổng cộng </b></td>
                            <td class="text-right">342.000 VNĐ</td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <h3>Lịch sử đơn hàng</h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left">Ngày tạo</td>
                            <td class="text-left">Tình trạng</td>
                            <td class="text-left">Ghi chú</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left">30/09/2018</td>
                            <td class="text-left">Đang xử lý</td>
                            <td class="text-left"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons clearfix button-box">
                    <div class="pull-right"><a href="http://9736.chilishop.net/index.php?route=account/order"
                                               class="btn btn-primary">Tiếp tục</a></div>
                </div>
                <div class="position-display">
                </div>
            </div>
        </div>
    </div>
@endsection