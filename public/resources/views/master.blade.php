<!DOCTYPE html>
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="vi">
<!--<![endif]-->

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Mẹ và Bé</title>
  <meta name="description" content="Children Toys" />
  <meta name="keywords" content="Children Toys" />
  <meta http-equiv="X-U
    A-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
  @php
    $favicon = Helper::getMeta('favicon');
  @endphp
  <link href="uploads/{{ $favicon }}" rel="icon" />
  <script src="resources/views/catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <link href="resources/views/catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
  <script src="resources/views/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="resources/views/catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="resources/views/catalog/view/javascript/jquery/owl-carousel/owl.carousel.tabs.css" type="text/css" rel="stylesheet" media="screen" />
  <link href="resources/views/system/config/revslider/rs-plugin/css/settings.css" type="text/css" rel="stylesheet" media="screen" />
  <link href="resources/views/system/config/revslider/rs-plugin/css/static-captions.css" type="text/css" rel="stylesheet" media="screen" />
  <link href="resources/views/system/config/revslider/rs-plugin/css/dynamic-captions.css" type="text/css" rel="stylesheet" media="screen" />
  <link href="resources/views/system/config/revslider/rs-plugin/css/captions.css" type="text/css" rel="stylesheet" media="screen" />
  <link rel="stylesheet" href="resources/views/admin/libs/toastr.min.css">
  <script src="resources/views/admin/libs/jquery.min.js"></script>
  <script src="resources/views/admin/libs/toastr.min.js"></script>
  <script src="resources/views/system/config/revslider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
  <script src="js/CustomerAPI.js" type="text/javascript"></script>
  <script src="resources/views/system/config/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
  <!--//script zoom product-->
  <script src="resources/views/catalog/view/theme/Mb_Themes/js/jquery.scrollUp.min.js" type="text/javascript"></script>
  <script src="resources/views/catalog/view/theme/Mb_Themes/js/main.js" type="text/javascript"></script>
  <!-- end zoomproduct -->
  <script src="resources/views/catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
  <link href="resources/views/catalog/view/javascript/jquery/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet" media="screen" />
  <link href="resources/views/catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />
  <link href="resources/views/catalog/view/theme/Mb_Themes/stylesheet/stylesheet.css" rel="stylesheet">
  <link href="resources/views/catalog/view/theme/Mb_Themes/stylesheet/d_quickcheckout.css" type="text/css" rel="stylesheet" media="screen">
  <link href="resources/views/catalog/view/theme/Mb_Themes/stylesheet/default.css" type="text/css" rel="stylesheet" media="screen">
  <link href="resources/views/catalog/view/theme/Mb_Themes/stylesheet/mb_setting.css" rel="stylesheet">
    <link href="resources/views/catalog/view/theme/Mb_Themes/stylesheet/styles.css" rel="stylesheet">
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments)
    };
    gtag('js', new Date());
    gtag('config', 'UA-60799117-7');
  </script>
  <link rel="stylesheet" href="resources/views/catalog/view/theme/Mb_Themes/stylesheet/animate.min.css" type="text/css" media="all" />
  <script src="js/StoreAPI.js" type="text/javascript"></script>
</head>

<body class="common-home">
  @include('header')
   @yield('content')


  @include('footer')
</body>

</html>
