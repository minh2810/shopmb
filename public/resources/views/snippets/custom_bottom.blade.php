<div class="custom-bottom" id="custom_bottom">
   <div class="container">
      <column class="position-display">
         <div>
            @php
            $id = explode(',', Helper::getMeta('special-collection'))[0];
            $special_collection = Helper::getCollection($id);
            @endphp
            <div class="show-in-tab-mod">
               <div class="non-show">
                  <div><i class="fa fa-frown-o"></i></div>
                  Xin lỗi, "Hiển thị danh mục trong tabs!" không thể hiển thị trong cột
               </div>
               <div class="show-in-tab">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#tab-189418-6" data-toggle="tab"
                        aria-expanded="true"><span>{{ $special_collection->title }}</span></a></li>
                  </ul>
               </div>
               <div class="tab-content row">
                  <div id="tab-189418-6" role="tabpanel" class="tab-pane active">
                     <div class="owl-car189418">
                       @php
                         $counter = 0;
                       @endphp
                        @foreach ($special_collection->products as $product)
                          @php
                            $counter++;
                          @endphp
                          @break($counter > 4)
                        <div class="product-layout-tab col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="product-thumb transition">
                              <div class="image">
                                 <a href="/product?id={{$product->id}}"><img
                                    src="/uploads/{{$product->featured_image}}"
                                    alt="" title="{{ $product->title }}" class="img-responsive"></a>
                              </div>
                              <div class="caption">
                                 <h4><a href="/product?id={{$product->id}}">{{ $product->title }}</a>
                                 </h4>
                                 <p class="description">{{ $product->description }}</p>
                                 <p class="price">{{Helper::formatMoney($product->price)}} VNĐ</p>
                              </div>
                              <div class="button-group-cart">
                                 <button type="button" data-toggle="tooltip" data-id="{{ $product->id }}"
                                    title="" data-original-title="Thêm vào giỏ">
                                 <span>Thêm vào giỏ</span></button>
                              </div>
                           </div>
                        </div>
                        @endforeach
                        @php
                          $counter = 0;
                        @endphp
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <script type="text/javascript"><!--
                  //Fix the product layout responsiveness
                  $(document).ready(function () {
                      $('.button-group-cart button').on('click', function () {
                          id = $(this).attr("data-id");
                          window.location.href = "/product?id=" + id;
                      })
                      //we only want this code to execute one time even if the are several showintabs mods in the pages
                      if (typeof showtabFLAG == 'undefined') {
                          //Set flag
                          showtabFLAG = true;

                          //Columns number
                          colsTab = $('#column-right, #column-left').length;

                          //default values for carousel
                          xsItems = 1;
                          smItems = 2;
                          mdItems = 4;
                          lgItems = 4;

                          //Check columns conditions
                          if (colsTab == 2) {
                              smItems = 1;
                              mdItems = 2;
                              lgItems = 2;
                              $('#content .product-layout-tab').attr('class', 'product-layout-tab product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
                              $('#content .product-layout-tab:nth-child(2n+2)').after('<div class="clearfix visible-md visible-sm"></div>');
                          } else if (colsTab == 1) {
                              mdItems = 3;
                              lgItems = 4;
                              $('#content .product-layout-tab').attr('class', 'product-layout-tab product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
                              $('#content .product-layout-tab:nth-child(3n+3)').after('<div class="clearfix visible-lg"></div>');
                          } else {
                              $('#content .product-layout-tab:nth-child(4n+4)').after('<div class="clearfix"></div>');
                          }
                      }
                  });
                  //-->
               </script>
            </div>
         </div>
         <div>
            @include('..snippets/brand')
        </div>
         <div>
            <div class="dv-builder-full">
               <div class="dv-builder line_ms">
                  <div class="dv-module-content">
                     <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                           <div class="dv-item-module ">
                              <div class="content_info follow-us">
                                 <ul class="list">
                                    <li class="item-content">
                                       <div class="item-description">
                                          Mạng xã hội:
                                       </div>
                                    </li>
                                    @php
                                    $youtub = Helper::getMeta('youtube');
                                    @endphp
                                    <li class="item-content">
                                       <a href="{{$youtub}}" target="_blank">
                                          <div class="item-image">
                                             <i class="fa icon-youtube"></i>
                                          </div>
                                       </a>
                                    </li>
                                    @php
                                    $twit = Helper::getMeta('twitter');
                                    @endphp
                                    <li class="item-content">
                                       <a href="{{ $twit }}" target="_blank">
                                          <div class="item-image">
                                             <i class="fa icon-twitter"></i>
                                          </div>
                                       </a>
                                    </li>
                                    @php
                                    $fb = Helper::getMeta('facebook');
                                    @endphp
                                    <li class="item-content">
                                       <a href="{{ $fb }}" target="_blank">
                                          <div class="item-image">
                                             <i class="fa icon-facebook"></i>
                                          </div>
                                       </a>
                                    </li>
                                    @php
                                    $rss = Helper::getMeta('rss');
                                    @endphp
                                    <li class="item-content">
                                       <a href="{{ $rss }}" target="_blank">
                                          <div class="item-image">
                                             <i class="fa icon-rss"></i>
                                          </div>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                           <div class="dv-item-module ">
                              <div class="content_info text_ms checks">
                                 <ul class="list">
                                    <li class="item-content">
                                       @php
                                       $service1 = Helper::getMeta('service_1');
                                       @endphp
                                       <div class="item-description">
                                          {{ $service1 }}
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                           <div class="dv-item-module ">
                              <div class="content_info text_ms text_ms2">
                                 <ul class="list">
                                    <li class="item-content">
                                       @php
                                       $service2 = Helper::getMeta('service_2');
                                       @endphp
                                       <div class="item-title">
                                          {{ $service2 }} <span class="title-child"></span>
                                       </div>
                                       @php
                                       $service3 = Helper::getMeta('service_3');
                                       @endphp
                                       <div class="item-description">
                                          {{ $service3 }}
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div>
            <div class="dv-builder-full">
               <div class="dv-builder list_product">
                  <div class="dv-module-content">
                     <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                           <div class="dv-item-module ">
                              <div class="product_module product_latest">
                                 <h3 class="title title_latest">Mới nhất</h3>
                                 @php
                                 $ids = array_slice(explode(',', Helper::getMeta('new-products')), 0, 2);
                                 $new_products = Helper::getProducts($ids);
                                 @endphp
                                 <div class="product-layout-custom">
                                    @foreach($new_products as $product)
                                    <div class="medium">
                                       <div class="product-thumb transition">
                                          <div class="image">
                                             <a
                                                href="/product?id={{ $product->id }}"><img
                                                src="/uploads/{{$product->featured_image}}"
                                                alt="{{ $product->title }}"
                                                title="{{ $product->title }}"
                                                class="img-responsive" style="width: 60px; height: 75px;"></a>

                                          </div>
                                          <div class="caption">
                                             <h4>
                                                <a href="/product?id={{ $product->id }}">{{ $product->title }}</a>
                                             </h4>
                                             <p class="description">{{ $product->description }}</p>

                                             <p class="price">
                                                <span class="price-old">{{Helper::formatMoney($product->price)}}
                                                VNĐ</span>
                                                <br>
                                                <span class="price-new">{{Helper::formatMoney($product->price_compare)}}
                                                VNĐ</span>
                                             </p>
                                          </div>
                                          <div class="button-group-cart">
                                             <button type="button" data-toggle="tooltip" title=""
                                                data-original-title="Thêm vào giỏ"><span>Thêm
                                             vào
                                             giỏ</span></button>
                                          </div>
                                       </div>
                                    </div>
                                    @endforeach
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                           <div class="dv-item-module ">
                              <div class="product_module product_bestseller">
                                 <h3 class="title title_bestseller"><span>Bán chạy<span></span></span></h3>
                                 @php
                                 $ids = array_slice(explode(',',Helper::getMeta('sale-products')), 0, 2);
                                 $sale_products = Helper::getProducts($ids);
                                 @endphp
                                 <div class="product-layout-custom">
                                    @foreach ($sale_products as $product)
                                    <div class="medium">
                                       <div class="product-thumb transition">
                                          <div class="image">
                                             <a
                                                href="/product?id={{ $product->id }}"><img
                                                src="/uploads/{{$product->featured_image}}"
                                                alt="{{ $product->title }}"
                                                title="{{ $product->title }}"
                                                class="img-responsive" style="width: 60px; height: 75px;"></a>

                                          </div>
                                          <div class="caption">
                                             <h4>
                                                <a href="/product?id={{ $product->id }}">{{ $product->title }}</a>
                                             </h4>
                                             <p class="description">{{ $product->description }}</p>

                                             <p class="price">
                                                <span class="price-old">{{Helper::formatMoney($product->price)}}
                                                VNĐ</span>
                                                <br>
                                                <span class="price-new">{{Helper::formatMoney($product->price_compare)}}
                                                VNĐ</span>
                                             </p>
                                          </div>
                                          <div class="button-group-cart">
                                             <button type="button" data-toggle="tooltip" title=""
                                                data-original-title="Thêm vào giỏ"><span>Thêm
                                             vào
                                             giỏ</span></button>
                                          </div>
                                       </div>
                                    </div>
                                    @endforeach
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                           <div class="dv-item-module ">
                              <div class="product_module special_product">
                                 <h3 class="title title_special">Khuyến mãi</h3>
                                 @php
                                 $ids = array_slice(explode(',', Helper::getMeta('promotion-products')), 0, 2);
                                 $promotion_products = Helper::getProducts($ids);
                                 @endphp
                                 <div class="product-layout-custom">
                                    @foreach($promotion_products as $product)
                                    <div class="medium">
                                       <div class="product-thumb transition">
                                          <div class="image">
                                             <a
                                                href="/product?id={{ $product->id }}"><img
                                                src="/uploads/{{$product->featured_image}}"
                                                alt="{{ $product->title }}"
                                                title="{{ $product->title }}"
                                                class="img-responsive" style="width: 60px; height: 75px;"></a>

                                          </div>
                                          <div class="caption">
                                             <h4>
                                                <a href="/product?id={{ $product->id }}">{{ $product->title }}</a>
                                             </h4>
                                             <p class="description">{{ $product->description }}</p>

                                             <p class="price">
                                                <span class="price-old">{{Helper::formatMoney($product->price)}}
                                                VNĐ</span>
                                                <br>
                                                <span class="price-new">{{Helper::formatMoney($product->price_compare)}}
                                                VNĐ</span>
                                             </p>
                                          </div>
                                          <div class="button-group-cart">
                                             <button type="button" data-toggle="tooltip" title=""
                                                data-original-title="Thêm vào giỏ"><span>Thêm
                                             vào
                                             giỏ</span></button>
                                          </div>
                                       </div>
                                    </div>

                                    @endforeach
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                           <div class="dv-item-module ">
                              <div class="content_info photos-flickr">
                                 <div class="title">
                                    <h3><span>HÌNH ẢNH INSTARGRAM</span></h3>
                                 </div>
                                 <ul class="list">
                                    <li class="item-content">
                                       <div class="item-image">
                                          <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635664408198490046-75x75.jpg">
                                       </div>
                                    </li>
                                    <li class="item-content">
                                       <div class="item-image">
                                          <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635666064112382505-75x75.jpg">
                                       </div>
                                    </li>
                                    <li class="item-content">
                                       <div class="item-image">
                                          <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635697175474516468-75x75.jpg">
                                       </div>
                                    </li>
                                    <li class="item-content">
                                       <div class="item-image">
                                          <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635701578765306357-75x75.jpg">
                                       </div>
                                    </li>
                                    <li class="item-content">
                                       <div class="item-image">
                                          <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635827619704125069-75x75.jpg">
                                       </div>
                                    </li>
                                    <li class="item-content">
                                       <div class="item-image">
                                          <img src="http://9736.chilishop.net/image/cache/catalog/banner/cm_b41424-75x75.jpg">
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </column>
   </div>
</div>
