if ((typeof StoreAPI) === 'undefined') {
  StoreAPI = {};
}
function money(num) {
  if (num) num = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  return num;
}

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

StoreAPI.addItem = function(id, quantity, callback) {
  var quantity = quantity || 1;
  var params = {
    type: 'POST',
    url: 'addToCart',
    data: {
      id: id,
      quantity: quantity
    },
    success: function(result) {
      if (typeof(callback) === 'function') {
        callback($.parseJSON(result));
      }
    }
  };
  $.ajax(params);
};

StoreAPI.removeItem = function(id, callback) {
  var params = {
    type: 'POST',
    url: 'removeItem',
    data: {
      id: id
    },
    success: function(result) {
      if (typeof(callback) === 'function') {
        callback($.parseJSON(result));
      }
    }
  }
  $.ajax(params);
}

StoreAPI.changeItem = function(id, quantity, callback) {
  var params = {
    type: 'POST',
    url: 'changeItem',
    data: {
      id: id,
      quantity: quantity
    },
    success: function(result) {
      if (typeof(callback) === 'function') {
        callback($.parseJSON(result));
      }
    }
  }
  $.ajax(params);
}

StoreAPI.getCart = function(callback) {
    jQuery.post('getCart', function(cart) {
        if ((typeof callback) === 'function') {
            callback($.parseJSON(cart));
        }
    });
}
