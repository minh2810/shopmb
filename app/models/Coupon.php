<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
  protected $table = 'coupon';

  public static function setStatus($data) {
    $coupon = Coupon::where('status', 'using')->where('id', $data->id)->get()->first();
    $coupon->status = $data->status;

    $order->updated_at = date('Y-m-d H:i:s');
    $order->save();
  }

  public static function insert($data) {
    $check = Coupon::where('status','!=','delete')->where('code',$data->code)->first();
    if($check) return -1;
    $coupon = new Coupon;
    $coupon->title = $data->title;
    $coupon->description = $data->description;
    $coupon->code = $data->code;
    $coupon->status = $data->status;
    $coupon->start_date = $data->start_date;
    $coupon->end_date = $data->end_date;
    if (time() > strtotime($coupon->end_date)) $coupon->status = 'expried';
    $coupon->created_at = date('Y-m-d H:i:s');
    $coupon->updated_at = date('Y-m-d H:i:s');
    $coupon->minimum = $data->minimum;
    $coupon->discount = $data->discount;
    $coupon->usage_left = $data->usage_left;
    $coupon->save();
    return $coupon->id;
  }

}
