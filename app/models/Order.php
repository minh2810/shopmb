<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table = 'order';

  public static function insert($data) {
    $order = new Order;
    $order->customer_id = $data->customer_id;
    $order->payment_method = $data->payment_method;
    $order->order_status = "new";
    $order->total = $data->total + 20000;
    $order->discount = $data->discount;

    $order->receiver_name = $data->receiver_name;
    $order->receiver_phone = $data->receiver_phone;
    $order->receiver_address = $data->receiver_address;

    $order->created_at = date('Y-m-d H:i:s');
    $order->updated_at = date('Y-m-d H:i:s');

    $saved = $order->save();
    if(!$saved){
        App::abort(500, 'Error');
    }

    return $order->id;
  }

  public static function changeStatus($id, $status) {
    $order = Order::where('id', $id)->get()->first();
    $order->updated_at = date('Y-m-d H:i:s');
    $order->order_status = $status;
    $order->save();
    return $order->id;
  }

  public static function quickUpdate($data) {
      $order = Order::where('id', $data->id)->get()->first();
      $order->total = $data->total;
      $order->order_status = $data->order_status;
      $order->updated_at = date('Y-m-d H:i:s');
      $order->save();
    return $order->id;
  }

  public static function updateInfo($data)
  {
    $order = \App\Models\Order::where('id', $data->id)->get()->first();
    $order->receiver_name = $data->receiver_name;
    $order->receiver_phone = $data->receiver_phone;
    $order->receiver_address = $data->receiver_address;
    $order->updated_at = date('Y-m-d H:i:s');
    $order->update();
    return $order->id;
  }
}
