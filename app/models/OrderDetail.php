<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
  protected $table = 'order_detail';

  public static function insert($data) {
    $order_detail = new OrderDetail;
    $order_detail->order_id = $data->order_id;
    $order_detail->product_id = $data->id;
    $order_detail->quantity = $data->quantity;
    $order_detail->unit_price = $data->price;
    $order_detail->created_at = date('Y-m-d H:i:s');
    $order_detail->updated_at = date('Y-m-d H:i:s');

    $saved = $order_detail->save();
    if (!$saved) {
      App::abort(500, 'Error');
    }
    return $order_detail->id;
  }
}
