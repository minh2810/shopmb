<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table = 'meta';

    public static function insert($data) {
      $meta = new Meta;
      if (!empty($check = Meta::where('name', $data->name)->get()->first())) {
          $meta = $check;
      }
      $meta->name = $data->name;
      $meta->value = $data->value;
      $meta->created_at = date('Y-m-d H:i:s');
      $meta->updated_at = date('Y-m-d H:i:s');
      if (!empty($check)) {
        $meta->update();
      } else {
        $meta->save();        
      }
    }
}
