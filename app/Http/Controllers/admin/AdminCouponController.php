<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Coupon;
use App\Models\Order;

class AdminCouponController extends \App\Http\Controllers\Controller
{
  public function getCoupons(Request $request) {
    if ($request->session()->has('admin')) {
      $coupon_query = Coupon::orderBy('created_at', 'desc');
      $page_name = "Tất cả";
      if (strpos($request->sort, 'only') !== false) {
        $coupon_query = Coupon::where('status', explode('-', $request->sort)[1])->orderBy('created_at', 'desc');
        $page_name = explode('-', $request->sort)[1];
      }
      $page = $request->page ? $request->page : 1;
      $perpage = $request->perpage ? $request->perpage : 8;
      $total_page = $coupon_query->count() % $perpage > 0 ? intval($coupon_query->count() / $perpage) + 1 : intval($coupon_query->count() / $perpage);
      $total_item = $coupon_query->count();
      $skip = ($page - 1) * $perpage;
      $coupons = $coupon_query->skip($skip)->take($perpage)->get();
      $current_item = count($coupons);
      return view('admin.coupon', [
        'current_page'=>'admin/coupon_all',
        'coupons'=>$coupons,
        'page'=>$page,
        'perpage'=>$perpage,
        'total_page'=>$total_page,
        'current_items'=>$current_item,
        'total_item'=>$total_item,
        'page_name'=>$page_name
      ]);
    } else {
      return view('admin.login');
    }
  }
  public function getCreateCoupon(Request $request) {
    if ($request->session()->has('admin')) {
      return view('admin.coupon_create', ['current_page'=>'admin/coupon_all']);
    } else {
      return view('admin.login');
    }
  }

  public function createCoupon(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->title = $request->title;
      $data->description = $request->description;
      $data->code = $request->code;
      $data->status = $request->status;
      $data->start_date = $request->start_date;
      $data->end_date = $request->end_date;
      $data->minimum = $request->min_value_order;
      $data->discount = $request->value;
      $data->usage_left = $request->usage_left;
      $coupon_id = Coupon::insert($data);
      return response(['code' => 0, 'status' => 'success', 'result' => $data], 200)->header('Content-Type', 'text/plain');
    } else {
      return view('admin.login');
    }
  }
}