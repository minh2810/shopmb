<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Coupon;

class CouponController extends Controller
{
  public static function checkStatus(Request $request, Response $response)
  {
    $code = $request->code;
    $cart = Helper::getCartFromSession();
    $coupon = Coupon::where('status', 'active')->where('code', $code)->get()->first();
    if (!empty($coupon)) {
      $message = "";
      $current_date = date('Y-m-d');
      if ($coupon->usage_left <= 0) {
        $message = "Mã giảm giá đã hết số lần sử dụng";
        return response(['code' => -1, 'status' => 'fail', 'message'=>$message, 200])->header('Content-Type', 'text/plain');
      } else if ($coupon->end_date < $current_date) {
        $message = "Mã giảm giá hết hạn sử dụng";
        return response(['code' => -1, 'status' => 'fail', 'message'=>$message, 200])->header('Content-Type', 'text/plain');
      }
      else if ($coupon->start_date > $current_date) {
        $message = "Mã giảm giá chưa có hiệu lực sử dụng";
        return response(['code' => -1, 'status' => 'fail', 'message'=>$message, 200])->header('Content-Type', 'text/plain');
      }
      else {
        $message = "Áp dụng thành công mã giảm";
        return response(['code' => 0, 'status' => 'success', 'message'=>$message ,'data' => $coupon, 200])->header('Content-Type', 'text/plain');
      }
    } else {
      $message = "Mã giảm không tồn tại";
      return response(['code' => -1, 'status' => 'fail', 'message'=>$message, 200])->header('Content-Type', 'text/plain');
    }
  }
}
