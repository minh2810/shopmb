<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Models\Order;
use \App\Models\Customer;
use \App\Models\Coupon;

class CheckoutController extends Controller
{
  public static function getCheckout(Request $request)
  {
    if (!$request->session()->has('cart') || count($request->session()->get('cart')->items) == 0) {
      return redirect('cart');
    }
    $cart = CartController::getCartFromSession($request);
    return view('content.checkout', ['cart' => $cart]);
  }

  public static function checkout(Request $request, Response $response)
  {
//    try {
      if (!$request->session()->has('customer')) {
        return redirect('checkout');
      }
      $cart = CartController::getCartFromSession($request);
      $customer = $request->session()->get('customer');
      $customer->name = $request->input('customer_name');
      $customer->email = $request->input('customer_email');
      $customer->phone = $request->input('customer_phone');
      $customer->address = $request->input('customer_address');
      Customer::updateCheckout($customer);
      $discount = 0;
      if (!empty($request->code)) {
        $discount = Coupon::where('status', 'using')->where('code', $request->code)->get()->first()->discount;
      }
      $order = (object)[
        'payment_method' => $request->input('payment_method'),
        'total' => $cart->total,
        'customer_id' => $request->session()->get('customer')->id,
        'receiver_name' => $request->input('receiver_name'),
        'receiver_phone' => $request->input('receiver_phone'),
        'receiver_address' => $request->input('receiver_address'),
        'discount' => $discount];
      $order_id = \App\Models\Order::insert($order);
      if ($request->session()->has("sidebar_list")) {
        $sidebar_list = $request->session()->get('sidebar_list')->keyBy('href');
        $sidebar_order = $sidebar_list->get('admin/order');
        $sidebar_order->counter = Order::where('order_status', 'new')->count();
      }
      foreach ($cart->items as $item) {
        $item->order_id = $order_id;
        \App\Models\OrderDetail::insert($item);
      }
      $request->session()->forget('cart');
//    } catch (\Throwable $e) {
//      error_log($e->getMessage());
//    }
    return response(['code' => 0, 'status' => 'success', 200])->header('Content-Type', 'text/plain');
  }

  public static function getSuccess(Request $request)
  {
    return view('content.success');
  }
}
